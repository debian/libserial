Source: libserial
Section: devel
Priority: optional
Homepage: https://github.com/crayzeewulf/libserial
Maintainer: Gianfranco Costamagna <locutusofborg@debian.org>
Uploaders: David Morris <dave@code-fish.co.uk>
Build-Depends: debhelper-compat (= 13),
               libgtest-dev <!nocheck>,
               libboost-test-dev <!nocheck>,
Build-Depends-Indep: python3-sphinx,
                     python3-sphinx-rtd-theme,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/libserial
Vcs-Git: https://salsa.debian.org/debian/libserial.git

Package: libserial1
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Serial port programming in C++ under POSIX operating system
 A collection of C++ classes which allow the serial port on POSIX systems to be
 accessed like an iostream object. Special functions are provided for setting
 various parameters of the serial port such as the baud rate, character size,
 flow control and other.
 .
 Provides the libserial library

Package: libserial-dev
Section: libdevel
Suggests: libserial-doc
Architecture: any
Depends: libserial1 (=${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: Serial port programming in C++ -- development files
 A collection of C++ classes which allow the serial port on POSIX systems to be
 accessed like an iostream object. Special functions are provided for setting
 various parameters of the serial port such as the baud rate, character size,
 flow control and other.
 .
 This package contains the development libraries and headers

Package: libserial-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: foreign
Description: Serial port programming in C++ -- documentation
 A collection of C++ classes which allow the serial port on POSIX systems to be
 accessed like an iostream object. Special functions are provided for setting
 various parameters of the serial port such as the baud rate, character size,
 flow control and other.
 .
 Provides the documentation for the library and development packages
